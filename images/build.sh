#!/bin/sh

BASEDIR=$(dirname "$0")
cd "$BASEDIR"
BASEDIR=$(pwd)


docker build -t smouquet/base:16.04 base/
docker build -t smouquet/mariadb:10.1 mariadb/
docker build -t smouquet/openjdk:8 openjdk/8/
docker build -t smouquet/postgres:9.6 postgres/
docker build -t smouquet/apache:2.4 apache/
docker build -t smouquet/php:7.0 php/
docker build -t smouquet/nextcloud:10.0.1 nextcloud/

