#!/bin/sh
set -e

if [ ! -e '/var/www/html/version.php' ]; then
    cp -R /usr/src/nextcloud/* /var/www/html
    chown -R www-data:www-data /var/www/html
#else
#    sudo -u www-data php7.0 /var/www/html/occ upgrade -v -n
fi

exec "$@"
