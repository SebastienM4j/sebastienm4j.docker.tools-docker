**Tools Docker**

Outillage pour Docker : images, exemple de docker compose, ...

Images
======

Liste des images (avec dépendances) :
-------------------------------------

* `base` : image de base d'après une Ubuntu 16.04
	* `mariadb` : serveur de base de données MariaDB 10.1
	* `openjdk` : kit de développement Java 8
	* `postgres` : serveur de base de données PostgreSQL 9.6
	* `apache` : serveur http Apache 2.4
		* `php` : module Php 7.0 sur un serveur Apache 2.4
			* `nextcloud` : application web de cloud personnel Nextcloud 10.0.1

base :
------

Image de base pour toutes les autres images Docker depuis une Ubuntu 16.04.

Paramétrée pour être en `fr_FR.UTF-8`.
Outils pré-installés :  bzip2, curl, sudo, vim

mariadb :
---------

Serveur de base de données MariaDB (fork de MySQL). Image inspirée de [l'image officielle](https://hub.docker.com/_/mariadb/)

Variables d'environnement :
* `MYSQL_ROOT_PASSWORD` : mot de passe du superuser root
* `MYSQL_DATABASE` : nom de la base de données à créer
* `MYSQL_USER` : nom d'utilisateur de la base de données créée
* `MYSQL_PASSWORD` : mot de passe de l'utilisateur de la base de données créée

Port exposé : `3306`

Volume possible : `/var/lib/mysql`

openjdk :
---------

Kit de développement Java (JDK).

postgres :
----------

Serveur de base de données PostgreSQL. Image inspirée de [l'image officielle](https://hub.docker.com/_/postgres/).

Variables d'environnement :
* `POSTGRES_DB` : nom de la base de données à créer
* `POSTGRES_USER` : nom d'utilisateur de la base de données créée
* `POSTGRES_PASSWORD` : mot de passe de l'utilisateur de la base de données créée

Port exposé : `5432`

Volume possible : `/var/lib/postgresql/data`

apache :
--------

Serveur HTTP Apache.

Port exposé : `80`

Volume possible : `/var/www/html`

Contient une page "index.html" dans "var/www/html" permettant de constater le fonctionnement du serveur.

php :
-----

Module PHP installé sur le serveur HTTP Apache.

Les modules suivants sont installés par défaut :
* php-sqlite3
* php-mysql
* php-pgsql
* php-zip
* php-xml
* php-mbstring
* php-gd
* php-curl

Port exposé : `80`

Volume possible : `/var/www/html`

Contient une page "info.php" dans "var/www/html" permettant de constater le fonctionnement du serveur et de connaitre les informations sur l'installation PHP.

nextcloud :
-----------

Application de cloud personnel Nextcloud, en PHP, installée sur un serveur Apache.

Port exposé : `80`

Volumes possibles :
* `var/www/html` : la totalité de l'application, ou plus finement :
* `var/www/html/apps` : les applications installées / modifiées
* `var/www/html/config` : la configuration
* `var/www/html/data` : les données (documents, fichiers, ...)

Scripts :
---------

* `images/build.sh` : compile toutes les images avec le tag `smouquet/{image}:{version}`.

Compose :
=========

Liste des docker-compose :
--------------------------

* `nextcloud-mariadb` : exemple d'un Nextcloud avec une base MariaDB
* `nextcloud-postgres` : exemple d'un Nextcloud avec une base PostgreSQL

Command-line cheat sheet
========================

Docker engine :
---------------

* Construire une image : `docker build -t smouquet/{nom-image}[:version] images/{nom-image}`
* Créer un conteneur à partir d'une image : `docker run -d [-p {host-port}:{container-port}] [-v {host-absolute-path}:{container-absolute-path} smouquet/{nom-image}`
* Obtenir un shell sur un conteneur : `docker exec -i -t {container-id} /bin/bash`
* Copier des fichiers d'un conteneur vers l'hôte : `docker cp {container-id}:{container-absolute-path} {host-path}`
* Liste des conteneurs : `docker ps` ou `docker ps -a` pour avoir ceux arrếtés également
* Liste des images : `docker images`
* Supprimer un conteneur : `docker rm {container-id}`
* Supprimer une image : `docker rmi {image-id}`
* Arrêter/démarrer un conteneur : `docker stop/start {container-id}`

Docker compose :
----------------

* Contruire et lancer un compose : `docker-compose up`
* Arrêter et supprimer les conteneurs : `docker-compose down`

